<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// -------------------- tarjetas -----------------------------
Route::get('tarjetas/listar/{id}','TarjetasController@listar'); // http://127.0.0.1:8000/api/tarjetas/listar/1
Route::get('tarjetas/listarTodas','TarjetasController@listarTodas'); // http://127.0.0.1:8000/api/tarjetas/http://127.0.0.1:8000/api/tarjetas/listarTodas
Route::post('tarjetas/crear', 'TarjetasController@crearTarjeta');   // 127.0.0.1:8000/api/tarjetas/crear/   body-> urlencoded : cupoTotal, clave


// -------------------- transacciones -----------------------------
Route::post('transacciones/pagar/','TransaccionesController@pagar'); // 127.0.0.1:8000/api/transacciones/pagar/  body->urlencoded : tarjeta, valor, clave
/* igonrar esta-> es de pruebas */Route::post('transacciones/actualizarSaldo/','TransaccionesController@actualizarSaldo');
Route::get('transacciones/listar/{idTarjeta}','TransaccionesController@listar'); // http://127.0.0.1:8000/api/transacciones/listar/2
Route::get('transacciones/listarTodas/','TransaccionesController@listarTodas'); // http://127.0.0.1:8000/api/transacciones/listarTodas