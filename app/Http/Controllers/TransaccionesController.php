<?php

namespace App\Http\Controllers;

use App\transacciones;
use App\tarjetas;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class TransaccionesController extends Controller
{
  

    public function pagar(Request $request){
        // debe recibir $cupoTotal,$clave
        $transaccion = new transacciones();
        $transaccion -> fk_tarjeta = $request->tarjeta;
        $transaccion -> transaccion = $request->valor;

        if($this->validarClave($request->tarjeta , $request->clave) == 'true'){
            
            $this-> actualizarSaldo($request->tarjeta, $request->valor);
            
            $respuesta = $transaccion->save();
            $response = array(
                'success'=> $respuesta,
                'msg' => 'transaccion Creada'
            );

            return $response;
        }else{
            $response = array(
                'success'=> 'false',
                'msg' => 'clave incorrecta'
            );
            return $response;
        }
    }

    public function validarClave($nTarjeta,$clave){

        $tarjeta = new tarjetas();

        $resultado =  $tarjeta::where('id', $nTarjeta)->get();

        // si es la clave correcta retorna true
        if($resultado[0]['clave'] ==  $clave ){
            return 'true';
        }else{
            return 'false';
        }

    }

    
    public function actualizarSaldo($nTarjeta,$valor){

        /*$tarjeta = $request->$nTarjeta;
        $monto = $request->$monto;*/
        $tarjeta = new tarjetas();
        $tarjeta = $tarjeta::find($nTarjeta);
        $newCupo = $tarjeta->cupoDisponible + (int)$valor;
        $tarjeta ->cupoDisponible = $newCupo;
        $tarjeta->save();

        return new JsonResponse($nTarjeta);
    }

    public function listar($id){
        $transaccion = new transacciones();
        $response =  $transaccion::where('fk_tarjeta', $id)->get();
        return new JsonResponse($response);
    }

    public function listarTodas(){
        $transaccion = new transacciones();
        return new JsonResponse($transaccion::all());
    }


   

}
