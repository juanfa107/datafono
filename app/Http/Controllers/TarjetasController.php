<?php

namespace App\Http\Controllers;

use App\tarjetas;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;


class TarjetasController extends Controller
{   

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listar($id){
        $tarjeta = new tarjetas();
        $response =  $tarjeta::where('id', $id)->get();
        return new JsonResponse($response);
    }

    public function crearTarjeta(Request $request){
        // debe recibir $cupoTotal,$clave
        $tarjeta = new tarjetas();
        $tarjeta -> cupoTotal = $request->cupoTotal;
        $tarjeta -> cupoDisponible = $request->cupoTotal;
        $tarjeta -> clave      = $request->clave;
        $respuesta = $tarjeta->save();
        $response = array(
            'success'=> $respuesta,
            'msg' => 'Tarjeta Creada'
        );
        return new jsonResponse($response);
    }

    public function listarTodas(){
        $tarjeta = new tarjetas();

        return new JsonResponse($tarjeta::all());
    }



}
